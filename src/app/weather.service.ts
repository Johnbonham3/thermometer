import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(public http: HttpClient) { }

  public getWeather(latitude: string, longitude: string){
    const url = environment.apiUrl + 
    'lat=' + latitude + 
    '&lon=' + longitude +
    '&units=metric' +
    '&appid=' + environment.aipKey;

    return new Promise((resolve, reject) => {
      this.http.get(url).subscribe(data => {
        resolve(data);
      }, error => {
        reject('Error retrieving weather data.');
      });
    });
  }
}
